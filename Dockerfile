FROM nginx:alpine as webserver
COPY src /app
COPY docker /docker
RUN cp /docker/nginx/default.conf /etc/nginx/conf.d/default.conf \
    && cp /docker/nginx/nginx.conf /etc/nginx/nginx.conf

FROM composer as composer

FROM php:7.2-fpm-alpine as php-fpm
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY --from=webserver /app /app

WORKDIR /app
RUN apk add --no-cache $PHPIZE_DEPS \
    && pecl install redis \
    && composer install \
    && docker-php-ext-enable redis \
    && chown -R www-data:www-data /app/storage
