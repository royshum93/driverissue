Run the project
---------------
[![Build Status](https://img.shields.io/bitbucket/pipelines/royshum93/driverissue.svg)](https://bitbucket.org/royshum93/driverissue/addon/pipelines/home)

1. Copy `src/.env.prod` file to `src/.env`, and update the value (`GOOGLE_API_KEY`)
```
cp src/.env.prod src/.env
vi src/.env
```

2. Build
```
docker-compose -f docker-compose.yml build
```

3. Run (Default at port 80)
```
docker-compose up
```

4. Deploy to Swarm (Deploy to port 8080)
```
docker stack deploy -c docker-compose.yml my-stack
docker service update my-stack_webserver --publish-add 8080:80
```

Test
----
Test report is available at `src/storage/logs/coverage` after running test command.
```
docker-compose up composer
docker-compose up test
```