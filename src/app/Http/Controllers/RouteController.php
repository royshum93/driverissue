<?php

namespace App\Http\Controllers;

use App\Jobs\RouteQuery;
use App\Helpers\CoordinatesValidator;
use App\Helpers\Message;
use App\Repository\QueryRepository;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    /**
     * Process new search route request from HTTP POST
     *
     * @param Illuminate\Http\Request $request
     * @param App\Helpers\CoordinatesValidator $validator
     * @param App\Repository\QueryRepository $queryRepo
     * @return Illuminate\Http\Response
     */
    public function search(Request $request, CoordinatesValidator $validator, QueryRepository $queryRepo)
    {
        $rawData = json_decode($request->getContent(), true);
        if (empty($rawData) || !is_array($rawData) || !$validator->isValidList($rawData)) {
            return response()->json([Message::ERROR => Message::ERROR_DESC]);
        }
        
        $id = $queryRepo->create();
        dispatch(new RouteQuery($id, $rawData));
        
        return response()->json(['token' => $id]);
    }
    
    
    /**
     * Get Route Processing Result
     * 
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function show(string $id, QueryRepository $queryRepo)
    {
        $result = $queryRepo->get($id);
        if (!$result) {
            abort(404);
        }
        
        return response()->json($result);
    }
}
