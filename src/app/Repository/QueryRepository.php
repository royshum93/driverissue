<?php

namespace App\Repository;

use App\Helpers\Message;
use Cache;
use Illuminate\Support\Str;

class QueryRepository
{
    /**
     * Raise a new Query record
     * 
     * @return string ID of the query
     */
    public function create()
    {
        $uuid = (string) Str::uuid();
        Cache::forever($uuid, [Message::STATUS => Message::STATUS_PENDING]);
        
        return $uuid;
    }
    
    
    /**
     * Get the content of Query record by ID
     * 
     * @param string $id
     * @return array|null
     */
    public function get(string $id)
    {
        return Cache::get($id);
    }
    
    
    /**
     * Put the query result
     * 
     * @param string $id
     * @param array  $result
     * @return void
     */
    public function put(string $id, array $result)
    {
        Cache::forever($id, $result);
    }
}
