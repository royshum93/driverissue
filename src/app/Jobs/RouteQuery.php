<?php

namespace App\Jobs;

use App\Helpers\RouteQueryProcessor;
use App\Helpers\Message;
use App\Repository\QueryRepository;
use Cache;

class RouteQuery extends Job
{
    /**
     * @var $id
     */
    private $id;
    
    /**
     * @var array
     */
    private $locations;

    
    /**
     * Create a new RouteQuery instance.
     *
     * @return void
     */
    public function __construct(string $id, array $loc)
    {
        $this->id = $id;
        $this->locations = $loc;
    }
    

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RouteQueryProcessor $processor, QueryRepository $queryRepo)
    {
        $routeInfo = $processor->compute($this->locations);
        if (empty($routeInfo)) {
            $response = [
                    Message::STATUS => Message::STATUS_FAILURE,
                    Message::ERROR  => Message::ERROR_DESC 
                ];
            
            $queryRepo->put($this->id, $response);
            return;
        }
        
        $routeInfo = array_merge([Message::STATUS => Message::STATUS_OK], $routeInfo);
        $queryRepo->put($this->id, $routeInfo);
    }
    
    
    /**
     * Get ID of the job
     * 
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
