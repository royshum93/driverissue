<?php

namespace App\Helpers;

class CoordinatesValidator
{
    /**
     * Check if the given coordinate list is valid
     * 
     * @param string $coord
     * @return boolean
     */
    public function isValidList(array $coordinates)
    {
        if (count($coordinates) < 2 || array_keys($coordinates) !== range(0, count($coordinates) - 1)) {
            return false;
        }
        
        foreach ($coordinates as $index => $raw) {
            if (
                empty($raw) 
                || !is_array($raw)
                || count($raw) !== 2
                || !$this->isValidCoordinate($raw[0])
                || !$this->isValidCoordinate($raw[1])
                ) {
                return false;
            }
        }
        
        return true;
    }
    
    
    /**
     * Check if the given coordinate is valid
     * 
     * @param string $coord
     * @return boolean
     */
    private function isValidCoordinate(string $coord)
    {
        if (!is_numeric($coord)) {
            return false;
        }
        
        $coord = floatval($coord);
        
        return !(abs($coord) > 180);
    }
}
