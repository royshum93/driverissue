<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class GoogleMapsDirectionAPI
{
    const API_ENDPOINT = 'https://maps.googleapis.com/maps/api/directions/json?';
    
    /**
     * @var GuzzleHttp\Client
     */
    private $client;
    
    
    /**
     * Construct API instance by inject Http Client
     * 
     * @param GuzzleHttp\Client $client
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    
    
    /**
     * Get Route Info from Google Direction API
     * 
     * @param array $startEndInfo
     * @return array|boolean
     */
    public function get(array $startEndInfo)
    {
        $key = env('GOOGLE_API_KEY');
        $makeCoord = function (array $coord) {
                        return $coord[0] .',' .$coord[1];
                    };
        
        $origin = $makeCoord(array_shift($startEndInfo));
        $waypoints = array_map($makeCoord, $startEndInfo);
        $destination = array_pop($waypoints);
        $waypoints = join('|', array_merge(['optimize:true'], $waypoints));
        $alternatives = 'true';
        
        $query = compact('origin', 'destination', 'alternatives', 'key');
        
        if (!empty($waypoints)) {
            $query['waypoints'] = $waypoints;
        }
        
        $response = $this->client->get(self::API_ENDPOINT.http_build_query($query));
        if (200 !== $response->getStatusCode()) {
            return false;
        }
        
        return json_decode($response->getBody()->getContents(), true);
    }
}
