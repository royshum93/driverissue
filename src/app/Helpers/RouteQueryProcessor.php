<?php

namespace App\Helpers;

class RouteQueryProcessor
{
    /**
     * @var App\Helpers\GoogleMapsDirectionAPI
     */
    private $api;
    
    
    /**
     * Construct API instance by inject Http Client
     * 
     * @param GoogleMapsDirectionAPI $api
     * @return void
     */
    public function __construct(GoogleMapsDirectionAPI $api)
    {
        $this->api = $api;
    }
    
    
    /**
     * Get routes, compare and reorder route
     * 
     * @param array $startEndInfo
     * @return array|boolean
     */
    public function compute(array $startEndInfo)
    {
        $resultList = [];
        foreach ($startEndInfo as $index => $isFinalDropOff) {
            if (0 === $index) {
                continue;
            }
            
            $routes = $startEndInfo;
            unset($routes[$index]);
            $routes = array_values($routes);
            $routes[] = $isFinalDropOff;
            
            $result = $this->computeSingleRoute($routes);
            if (!empty($result)) {
                $resultList[] = $result;
            }
        }
        
        if (empty($resultList)) {
            return false;
        }
        
        $resultDistanceList = array_pluck($resultList, Message::TOTAL_DISTANCE);
        $minDistance = min($resultDistanceList);
        
        return $resultList[array_search($minDistance, $resultDistanceList)];
    }
    
    
    
    /**
     * Handle Single Route Logic
     * 
     * @param array $startEndInfo
     * @return array|boolean
     */
    private function computeSingleRoute(array $startEndInfo)
    {
        $response = $this->api->get($startEndInfo);

        if (
                empty($response['status']) || 
                'OK' !== $response['status'] || 
                empty($response['routes'])
        ) {
            return false;
        }
        
        $routes = [];
        foreach ($response['routes'] as $route) {
            if (!empty($route) && !empty($route['legs'])) {
                $routes[] = [
                                'distance' => array_sum(array_pluck($route['legs'], 'distance.value')),
                                'duration' => array_sum(array_pluck($route['legs'], 'duration.value')),
                                'startLocation' => array_pluck($route['legs'], 'start_location'),
                                'endLocation' => array_pluck($route['legs'], 'end_location'),
                            ];
            }
        }
        
        $distanceList = array_pluck($routes, 'distance');
        $totalDistance = min($distanceList);
        
        $route = $routes[array_search($totalDistance, $distanceList)];
        $totalTime = $route['duration'];
        
        $pathList = [];
        foreach ($route['startLocation'] as $step) {
                $pathList[] = [
                                number_format($step['lat'], 6),
                                number_format($step['lng'], 6)
                            ];
        }
        
        $endLocation = end($route['endLocation']);
        $pathList[] = [
                        number_format($endLocation['lat'], 6),
                        number_format($endLocation['lng'], 6)
                    ];
        
        return [
                Message::PATH => $pathList,
                Message::TOTAL_TIME => $totalTime,
                Message::TOTAL_DISTANCE => $totalDistance
            ];
    }
}
