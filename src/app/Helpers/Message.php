<?php

namespace App\Helpers;

class Message
{
    const ERROR = 'error';
    const ERROR_DESC = 'ERROR_DESCRIPTION';
    
    const PATH = 'path';
    
    const STATUS = 'status';
    const STATUS_FAILURE = 'failure';
    const STATUS_PENDING = 'in progress';
    const STATUS_OK = 'success';
    
    const TOTAL_TIME = 'total_time';
    const TOTAL_DISTANCE = 'total_distance';
}
