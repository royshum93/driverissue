<?php

namespace Test\Unit;

use Test\TestCase;
use App\Helpers\RouteQueryProcessor;
use App\Repository\QueryRepository;
use App\Jobs\RouteQuery;

class RouteQueryTest extends TestCase
{
    /**
     * @var App\Helpers\RouteQueryProcessor Stub
     */
    private $processorStub;
    
    /**
     * @var App\Jobs\RouteQuery
     */
    private $query;
    
    /**
     * @var App\Repository\QueryRepository Stub
     */
    private $repoStub;
    
    
    public function setUp()
    {
        parent::setUp();
        $this->processorStub = $this->createMock(RouteQueryProcessor::class);
        $this->repoStub = $this->createMock(QueryRepository::class);
        $this->query = new RouteQuery('test', [
                                ["22.323081", "114.134877"],
                                ["22.223419", "114.145610"],
                                ["22.322322", "114.163411"]
                            ]);
    }
    
    
    public function testGetId()
    {
        $this->assertEquals('test', $this->query->getId());
    }
    
    
    public function testHandleQueryJob()
    {
        $this->setProcessorExpectedReturn([
                        "path" => [
                            ["22.372081", "114.107877"],
                            ["22.326442", "114.167811"],
                            ["22.284419", "114.159510"]
                        ],
                        "total_distance" => 20000,
                        "total_time" => 1800
                    ]);

        $this->setRepoExpectedInput([
                        'status' => 'success',
                        "path" => [
                            ["22.372081", "114.107877"],
                            ["22.326442", "114.167811"],
                            ["22.284419", "114.159510"]
                        ],
                        "total_distance" => 20000,
                        "total_time" => 1800
                    ]);
                    
        $this->query->handle($this->processorStub, $this->repoStub);
    }
    
    
    public function testHandleQueryFailedAPI()
    {
        $this->setProcessorExpectedReturn(false);
        $this->setRepoExpectedInput([
                        'status' => 'failure',
                        'error' => 'ERROR_DESCRIPTION'
                    ]);
                    
        $this->query->handle($this->processorStub, $this->repoStub);
    }
    
    
    /**
     * Set processor return value
     * 
     * @param mixed $value
     * @return void
     */
    private function setProcessorExpectedReturn($value)
    {
        $this->processorStub->expects($this->once())
                ->method('compute')
                ->willReturn($value);
    }
    
    
    /**
     * Set Repository return value
     * 
     * @param mixed $value
     * @return void
     */
    private function setRepoExpectedInput($value)
    {
        $this->repoStub->expects($this->once())
                ->method('put')
                ->with($this->equalTo('test'), $this->equalTo($value));
    }
}
