<?php

namespace Test\Unit;

use Test\TestCase;
use App\Helpers\CoordinatesValidator;

class RouteControllerTest extends TestCase
{
    /**
     * @var App\Helpers\CoordinatesValidator
     */
    private $route; 
    
    
    public function setup()
    {
        parent::setUp();
        $this->validator = app(CoordinatesValidator::class);
    }
    
    
    public function testIsValidCoordinate()
    {
        $__isValidCoord = function(string $coord) {
                            return $this->isValidCoordinate($coord);
                        };
        
        $cases = [
                ["22.323081", true],
                ["-114.323081", true],
                ["0.000", true],
                ["222.323081", false],
                ["-1122.323081", false],
                ["", false],
                ["abc", false]
            ];
        
        foreach ($cases as $case) {
            $actual = $__isValidCoord->call($this->validator, $case[0]);
            $expected = $case[1];
            $this->assertEquals($expected, $actual);
        }
    }
    
    
    /**
     * @depends testIsValidCoordinate
     */
    public function testIsValidCoordinateList()
    {
        $cases = [
                    [
                        [
                            ["22.323081", "114.134877"],
                            ["22.223419", "114.145610"],
                            ["22.322322", "114.163411"]
                        ],
                        true
                    ],
                    [
                        [
                            "a" => ["22.323081", "114.134877"],
                            "b" => ["22.223419", "114.145610"],
                            "c" => ["22.322322", "114.163411"]
                        ],
                        false
                    ],
                    [
                        ["22.323081", "114.134877"],
                        false
                    ],
                    [
                        [],
                        false
                    ]
            ];    
        
        foreach ($cases as $case) {
            $actual = $this->validator->isValidList($case[0]);
            $expected = $case[1];
            $this->assertEquals($expected, $actual);
        }
    }
}
