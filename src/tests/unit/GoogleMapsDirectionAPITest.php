<?php

namespace Test\Unit;

use App\Helpers\GoogleMapsDirectionAPI;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Test\TestCase;

class GoogleMapsDirectionAPITest extends TestCase
{
    /**
     * @var App\Helpers\GoogleMapsDirectionAPI
     */
    private $api;
    
    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    
    public function setUp()
    {
        parent::setUp();
        
        $this->client = $this->getMockBuilder(Client::class)
                                ->setMethods(['request'])
                                ->getMock();
        
        $this->api = new GoogleMapsDirectionAPI($this->client);
    }
    
    
    public function testSuccessFulGetFromGoogle()
    {
        $sampleResponse = json_encode($this->getAPIResponse(0));
        $this->client->expects($this->once())
                ->method('request')
                ->with(
                    $this->equalTo('get'),
                    $this->equalTo(
                            'https://maps.googleapis.com/maps/api/directions/json?'
                            . 'origin='.urlencode('22.248039,114.167975').'&destination=' 
                            . urlencode('22.245914,114.169031').'&alternatives=true'
                            . (env('GOOGLE_API_KEY') ? '&key='.urlencode(env('GOOGLE_API_KEY')) : '').'&waypoints=' 
                            . urlencode('optimize:true|22.248878,114.163063|22.250333,114.168667')
                    )
                )
                ->will($this->returnValue(
                    new Response(200, [], $sampleResponse))
                );

        $response = $this->api->get([
                                ["22.248039", "114.167975"],
                                ["22.248878", "114.163063"],
                                ["22.250333", "114.168667"],
                                ["22.245914", "114.169031"]
                            ]);
        
        $this->assertNotEmpty($response);
        $this->assertArrayHasKey('status', $response);
        $this->assertArraySubset(['status' => 'OK'], $response);
    }
    
    
    /**
     * @depends testSuccessFulGetFromGoogle
     */
    public function testFailedGetFromGoogle()
    {
        $this->client->expects($this->once())
                ->method('request')
                ->will($this->returnValue(
                    new Response(404, [], ''))
                );
        
        $response = $this->api->get([
                                ["22.323081", "114.134877"],
                                ["22.223419", "114.145610"],
                                ["22.223429", "114.145611"],
                                ["22.322322", "114.163411"]
                            ]);
        $this->assertNotTrue($response);
    }
}
