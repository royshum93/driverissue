<?php

namespace Test\Unit;

use Test\TestCase;
use App\Helpers\GoogleMapsDirectionAPI;
use App\Helpers\RouteQueryProcessor;

class RouteQueryProcessorTest extends TestCase
{
    /**
     * @var App\Helpers\RouteQueryProcessor
     */
    private $processor;
    
    /**
     * @var App\Helpers\GoogleMapsDirectionAPI Stub
     */
    private $stub;
    
    /**
     * @var array
     */
    private $calledResponseList;
    
    
    public function setUp()
    {
        parent::setUp();
        $this->stub = $this->createMock(GoogleMapsDirectionAPI::class);
        $this->processor = new RouteQueryProcessor($this->stub);
        $this->calledResponseList = [];
    }
    
    
    public function testComputeRouteInfo()
    {
        $response = $this->callProcessor();
        
        // Refer to fake result in googleAPISample.json
        $expectedTime = 210 + 256 + 129;
        $expectedDistance = 887 + 1124 + 778;
        
        $this->assertNotEmpty($response);
        $this->assertArraySubset(['total_distance' => $expectedDistance], $response);
        $this->assertArraySubset(['total_time' => $expectedTime], $response);
        
        $this->assertArrayHasKey('path', $response);
        $this->assertNotEmpty($response['path']);
        
        $expectedRoute = [
                            ["22.248037", "114.167976"],
                            ["22.245912", "114.169036"],
                            ["22.248882", "114.163062"],
                            ["22.250346", "114.168660"]
                        ];
        $this->assertArraySubset($expectedRoute, $response['path']);
        
        $expectedAPICall = 3; // There is 3 points, excluding start
        $this->assertEquals(3, array_sum($this->calledResponseList), 'Call sequence for Google API is incomplete');
    }
    
    
    public function testComputeRouteWithErrorDesc()
    {
        $errResponse = $this->getFailedGoogleAPI();
        $response = $this->callProcessor($errResponse);
        
        $this->assertNotTrue($response);
    }
    

    public function testComputeRouteWithNoDistance()
    {
        $errResponse = $this->getAPIResponse();
        unset($errResponse['routes'][0]['legs'][0]['distance']);
        
        $response = $this->callProcessor($errResponse);
        
        $this->assertNotTrue($response);
    }
    
    
    /**
     * Call Processor by giving the return value from API
     * 
     * @param mixed $value Value that should be returned by mocked API Instance
     * @return mixed
     */
    private function callProcessor($value = null)
    {
        $callMap = [
                [
                    [
                        ["22.248039", "114.167975"],
                        ["22.250333", "114.168667"],
                        ["22.245914", "114.169031"],
                        ["22.248878", "114.163063"]
                    ],
                    [
                        ["22.248039", "114.167975"],
                        ["22.245914", "114.169031"],
                        ["22.250333", "114.168667"],
                        ["22.248878", "114.163063"]
                    ],
                    [
                        ["22.248039", "114.167975"],
                        ["22.245914", "114.169031"],
                        ["22.248878", "114.163063"],
                        ["22.250333", "114.168667"]
                    ],
                    [
                        ["22.248039", "114.167975"],
                        ["22.248878", "114.163063"],
                        ["22.245914", "114.169031"],
                        ["22.250333", "114.168667"]
                    ],
                    [
                        ["22.248039", "114.167975"],
                        ["22.248878", "114.163063"],
                        ["22.250333", "114.168667"],
                        ["22.245914", "114.169031"]
                    ],
                    [
                        ["22.248039", "114.167975"],
                        ["22.250333", "114.168667"],
                        ["22.248878", "114.163063"],
                        ["22.245914", "114.169031"]
                    ]
                ],
                [
                    $this->getAPIResponse(0),
                    $this->getAPIResponse(0),
                    $this->getAPIResponse(1),
                    $this->getAPIResponse(1),
                    $this->getAPIResponse(2),
                    $this->getAPIResponse(2)
                ]
            ];
        
        $stub = $this->stub->expects($this->atLeastOnce())->method('get');
        if ($value) {
            $stub->willReturn($value);
        } else {
            $stub->will($this->returnCallback(function($arg) use ($callMap) {
                    $arg = json_encode($arg);
                    
                    foreach ($callMap[0] as $index => $input) {
                        if (json_encode($input) === $arg) {
                            $this->calledResponseList[] = floor($index / 2);
                            return $callMap[1][$index];
                        }
                    }
                    
                    return null;
                }));
        }
        
        return $this->processor->compute([
                                ["22.248039", "114.167975"],
                                ["22.248878", "114.163063"],
                                ["22.250333", "114.168667"],
                                ["22.245914", "114.169031"]
                            ]);
    }
}
