<?php

namespace Test\Unit;

use App\Repository\QueryRepository;
use Test\TestCase;

class QueryRepositoryTest extends TestCase
{
    /**
     * @var string
     */
    private $id;
    
    /**
     * @var App\Repository\QueryRepository
     */
    private $queryRepo;
    
    
    public function setUp()
    {
        parent::setUp();
        
        $this->queryRepo = app()->make(QueryRepository::class);
        $this->id = $this->queryRepo->create();
    }
    
    public function testCreateQueryAndGetQueryID()
    {
        $this->assertNotEmpty($this->id);
        $this->assertEquals(32 + 4, strlen($this->id), 'Expected length of $id is 32 + 4 (Length of UUID)');
        
        $data = $this->queryRepo->get($this->id);
        $this->assertArraySubset(['status' => 'in progress'], $data);
    }
    
    
    /**
     * @depends testCreateQueryAndGetQueryID
     */
    public function testStoreQueryResult()
    {
        $this->queryRepo = app()->make(QueryRepository::class);
        $this->id = $this->queryRepo->create();
        
        $this->queryRepo->put($this->id, ['status' => 'error']);
        
        $data = $this->queryRepo->get($this->id);
        $this->assertArraySubset(['status' => 'error'], $data);
    }
}
