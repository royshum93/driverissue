<?php

namespace Test;

abstract class TestCase extends \Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
    
    
    /**
     * Get Sample Google Direction API Output
     * 
     * @param integer Test Response Set#
     * @return string
     */
    protected function getAPIResponse($setNo = 0)
    {
        $json = file_get_contents(__DIR__.'/googleAPISample.json');
        
        return json_decode($json, true)[$setNo];
    }
    
    
    /**
     * Get Failed Google API Output
     * 
     * @return string
     */
    protected function getFailedGoogleAPI()
    {
        $json = '{"status":"ZERO_RESULTS","routes":[]}';
        
        return json_decode($json, true);
    }
}
