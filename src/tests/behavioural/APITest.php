<?php

namespace Test\Behavioural;

use App\Jobs\RouteQuery;
use Illuminate\Support\Facades\Bus;
use Test\TestCase;

class APITest extends TestCase
{
    /**
     * API Call with Correct input
     *
     * @return void
     */
    public function testSuccessRoute()
    {
        $this->json('POST', '/route', [
                                ["22.323081", "114.134877"],
                                ["22.223419", "114.145610"],
                                ["22.322322", "114.163411"]
                            ])
                ->assertResponseOk();

        $response = $this->seeJsonStructure(['token'])
                            ->response
                            ->getContent();
        $response = json_decode($response, true);

        //$this->expectsJobs('App\Jobs\RouteQuery');
        
        $this->json('GET', '/route/' .$response['token'])
                ->assertResponseOk();
        
        $this->seeJsonStructure(['status']);
    }
    
    
    /**
     * API Call with No Destination input
     * 
     * @return void
     */
    public function testNoDestination()
    {
        Bus::fake();
        
        $this->json('POST', '/route', [
                                ["22.323081", "114.134877"]
                            ])
            ->seeJson(["error" => "ERROR_DESCRIPTION"]);
            
        Bus::assertNotDispatched(RouteQuery::class);
    }
    
    
    /**
     * API Call with Empty Array input
     * 
     * @return void
     */
    public function testEmptyArrayInput()
    {
        Bus::fake();
        
        $this->json('POST', '/route', [])
            ->seeJson(["error" => "ERROR_DESCRIPTION"]);
            
        Bus::assertNotDispatched(RouteQuery::class);
    }
    
    
    public function testShowId()
    {
        $this->json('GET', '/route/abc')
                ->assertResponseStatus(404);
    }
}
